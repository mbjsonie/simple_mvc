$(document).ready( function() {
  var defaultError = 'Action could not be performed, please try again';
  var fieldsRequiredError = 'All fields are required';
  var init = function(){
    //key events
    $('#LogInPassword').live('keypress', function(e) {
      var keycode = e.keyCode ? e.keyCode : e.which;
      if(keycode === 13) {
        $('#LogIn').trigger("click");
      }
    });

    $('#alertClose').live('click', function() {
      closeAlertBox();
    });
    
    $('#LogIn').live( 'click', function(e) {
      e.stopImmediatePropagation();
      
      var email = $('#LogInEmail').val();
      var password = $('#LogInPassword').val();

      if(!email || !password) {
        showAlertBox(fieldsRequiredError,'error');
      }

      $.post("/users/login",
      {
        Email: email,
        Password: password
      },
      function(data) {
        if(!data || data.StatusCode !== 0) {
          if(data.msg) {
            showAlertBox(data.msg,'error');
          } else {
            showAlertBox(defaultError,'error');
          }
        } else {
          window.location.replace("/");
        }
      },
      "json");
    });
    
    $('#SignUp').live( 'click', function(e) {
      e.stopImmediatePropagation();
      
      var email = $('#SignUpEmail').val(),
          password = $('#SignUpPassword').val(),
          confirm = $('#SignUpConfirm').val();

      if(!email || !password) {
        showAlertBox(fieldsRequiredError,'error');
        return false;
      }
      
      if(password != confirm) {
        showAlertBox('Passwords do not match','error');
        return false;
      }
      
      $.post("/users/signUp",
      {
        Email: email,
        Password: password
      },
      function(data) {
        if(data.StatusCode === 0) {
          //Clear input
          $('#SignUpEmail').val('');
          $('#SignUpPassword').val('');
          $('#SignUpConfirm').val('');

          if(data.msg) {
            showAlertBox(data.msg,'success');
          }
        } else {

          if(data.msg) {
            showAlertBox(data.msg,'error');
          } else {
            showAlertBox(defaultError,'error');
          }
        }
      },
      "json");
    });
  }

  var viewport = function()
  {
    var e = window, a = 'inner';
    if ( !( 'innerWidth' in window ) ) {
      a = 'client';
      e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
  }
  var centerAlertBox = function() {
    var viewPort = viewport();
    $('#alertBox').css('top', (viewPort.height/2 - 20)+'px');
    $('#alertBox').css('left', (viewPort.width/2 - 150)+'px');
  }
  var showAlertBox = function(msg,type) {
    centerAlertBox();
    
    $('#alert').removeClass('alert-error').removeClass('alert-success').removeClass('alert-info');
    
    if(type != '') {
      $('#alert').addClass('alert-'+type);
    }
    
    $('#alertMsg').html(msg);
    $('#alertBox').css('display', 'block');
  }
  var closeAlertBox = function() {
    $('#alertBox').css('display', 'none');
  }
  
  $('.getFocus').focus();
  init();
  centerAlertBox();
});

