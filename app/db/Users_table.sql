CREATE TABLE IF NOT EXISTS `Users` (
  `UserId` int(8) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `DateJoined` datetime NOT NULL,
  `EmailConfirmed` bit(1) NOT NULL,
  `ConfirmationCode` varchar(16) CHARACTER SET utf8 NOT NULL,
  `LastActivity` datetime NOT NULL,
  `FailedLogins` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin ;