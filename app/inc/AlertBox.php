<?php

class AlertBox
{
  private static
  $alerts = array(  0   => null,
                    200 => array( 'statusCode'  => 200,
                                  'msg'         => 'Your email has been confirmed, you may now login',
                                  'type'        => 'success'
                                ),
                    999 => array( 'statusCode'  => 999,
                                  'msg'         => 'An unknown error occurred, please contact us at support@socialuw.com',
                                  'type'        => 'error'
                                )
                  );

  public static function getAlert($statusCode=0)
  {
    return self::$alerts[$statusCode];
  }
}

?>
