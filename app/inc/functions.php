<?php
function myUrl($url='',$fullurl=false)
{
  $s=$fullurl ? WEB_DOMAIN : '';
  $s.=WEB_FOLDER.$url;
  return $s;
}

function isLogedIn()
{
  if(isset($_SESSION['User']))
    return true;
  else
    return false;
}

function accessControl()
{
  if(!isset($_SESSION['User']))
  {
    header('Location: /users');
    exit;
  }
}

function redirectIfLogedIn()
{
  if(isset($_SESSION['User']))
  {
    header('Location: /');
    exit;
  }
  else
  {
    if(isset($_COOKIE['e']) && isset($_COOKIE['p']))
    {
      require(APP_PATH.'controllers/users/login.php');
      if(login($_COOKIE['e'],$_COOKIE['p']))
      {
        header('Location: /');
        exit;
      }
    }
  }
}

function getSQLDateTime()
{
  return date("Y-m-d H:i:s");
}

function genRandomString($length=10)
{
  $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
  $characterCount = strlen($characters) - 1;
  $string = '';  
    
  for ($i = 0; $i < $length; $i++)
  {
    $index = mt_rand(0, $characterCount);
    $string .= $characters[$index];
  }
  
  return $string;
}

function redirect($url,$alertmsg='')
{
  if ($alertmsg)
    addjAlert($alertmsg,$url);
  header('Location: '.myUrl($url));
  exit;
}

function passwordHash($password)
{
  $salt = $GLOBALS['HashSalt'];
  $hashedPassword = $salt . '::' . md5($salt . '::' . md5($password));
  return $hashedPassword;
}

function validEmail($email)
{
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
                 str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
      
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
      {
         // domain not found in DNS
         $isValid = false;
      }
   }
   return $isValid;
}
?>
