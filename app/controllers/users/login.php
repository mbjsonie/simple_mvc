<?php
function _login($Email=null,$Password=null)
{
  redirectIfLogedIn();
  
  if(isset($_POST['Email']) && $_POST['Email'] &&
    isset($_POST['Password']) && $_POST['Password'])
  {
    $Email = $_POST['Email'];
    $Password = $_POST['Password'];
  }
  
  $Password = passwordHash($Password);
  
  $msg = "";
  if(login($Email,$Password,$msg))
  {
    echo json_encode(array( "StatusCode"=>0));
  }
  else
  {
    echo json_encode(array( "StatusCode"=>1,
                        "msg"=>$msg));
  }
}

function login($Email=null,$Password=null,&$msg="")
{ 
  $user = new User();
  $user->retrieve_one("Email=? AND EmailConfirmed='1'",array($Email));
  
  if($user->exists())
  {
    if($user->get('FailedLogins') > 100)
    {
      $user->incrementFailedLogins();
      
      $msg = "Too many failed logins, account is currently locked. Please contact us to resolve the issue.";
      return false;
    }
    else if($user->get('Password') != $Password)
    {
      $user->incrementFailedLogins();
      
      $msg = "Password is incorrect";
      return false;
    }
    else
    {
      $user->resetFailedLogins();
      $user->updateActivity();
      
      $_SESSION['User'] = $user;
      
      $expire = time()+60*60*24*365; //1 year
      setcookie("e", $Email, $expire, "/");
      setcookie("p", $Password, $expire, "/");
      
      return true;
    }
  }
  $msg = "Account does not exist or is not active";
  return false;
}
