<?php
function _signUp($Email=null, $Password=null, $Faculty=null, $Sex=null)
{
  redirectIfLogedIn();
  
  if(isset($_POST['Email']) && $_POST['Email'] &&
    isset($_POST['Password']) && $_POST['Password'])
  {
    $Email = $_POST['Email'];
    $Password = $_POST['Password'];
  }
  else
  {
    header("Location: /");
    return;
  }
  
  if(!validEmail($Email))
  {
    echo json_encode(array( "StatusCode"=>1,
                    "msg"=>"Must be a valid email."));
    return;  
  }
  
  if(strlen($Password) < 5)
  {
    echo json_encode(array( "StatusCode"=>1,
                    "msg"=>"Password must be at least 5 characters long."));
    return;
  }
  
  $Password = passwordHash($Password);
  
  $User = new User();
  $SessionUser = $User->select(
      "UserId,Email,UserRole",
      "Email=?",array($Email));
  
  if($SessionUser != null)
  {
    echo json_encode(array( "StatusCode"=>1,
                    "msg"=>"User already exist."));
    return;
  }
  else
  {
    $Date = getSQLDateTime();
    $ConfirmationCode = genRandomString(10);
    
    $User = new User();
    
    $User->set('Email',$Email);
    $User->set('Password',$Password);
    $User->set('DateJoined',$Date);
    $User->set('EmailConfirmed',false);
    $User->set('ConfirmationCode',$ConfirmationCode);
    $User->set('LastActivity',$Date);
    
    $User->create();
    
    $Subject = 'Welcome to simpleMVC';
    $Body = "To verify your acount please visit: /users/confirm/$Email/$ConfirmationCode";
    $From = "From: noreply@mattsonier.com";
    
    if(mail($Email,$Subject,$Body,$From))
    {
      echo json_encode(array( "StatusCode"=>0,
                              "msg"=>"Your account has been created! Once you confirm your email, you will be able to login."));
    }
    else
    {
      echo json_encode(array( "StatusCode"=>1,
                              "msg"=>"We were unable to confirm your email. If it is a valid email, please contact me@mattsonier.com"));
    }
  }
}
