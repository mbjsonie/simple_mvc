<?php
function _confirm($Email=null, $ConfirmationCode=null)
{
  echo("called");
  
  if(!$Email || !$ConfirmationCode)
  {
    header("Location: /");
    return;
  }
  
  $user = new User();
  $user->retrieve_one("Email=?",array($Email));
  
  if(!$user->exists())
  {
    header("Location: /users/index/100");
  }
  elseif($user->get('ConfirmationCode') != $ConfirmationCode)
  {
    header("Location: /users/index/101");
  }
  else
  {
    $Date = getSQLDateTime();
    
    $user->set('EmailConfirmed',true);
    $user->set('LastActivity',$Date);
    $user->update();

    header("Location: /users/index/200");
  }
  
}
