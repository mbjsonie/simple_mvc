<?php
function _logout()
{
  $_SESSION = array();
  session_destroy();
  
  setcookie("e",'',time()-3600, "/");
  setcookie("p",'',time()-3600, "/");
  
  header("Location: /");
}
