<?php
function _index($orderByTrending = true,$NextVal=null)
{
  accessControl();
  
  $viewPath = APP_PATH.'views/layout.php';
  $User = $_SESSION['User'];

  $ViewConfig = array(  "PageType" => 'hiddenPage' );

  $view = new View($viewPath);
  $view->set('ViewConfig',$ViewConfig );
  $view->set('User',$User);
  $view->dump();
}
