<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $GLOBALS['sitename'];?></title>
    <meta name="description" content="">
    
    <link href="<?php echo WEB_FOLDER ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo WEB_FOLDER ?>css/style.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <?php
    $alertBox = null;
    if(isset($ViewConfig['msgCode']) && $ViewConfig['msgCode'])
    {
      $alertBox = AlertBox::getAlert($ViewConfig['msgCode']);
    }
  ?>
  

  <div id="alertBox" style="<?php echo ($alertBox ? 'display:block;' : 'display:none;'); ?>">
    <div id="alert" class="alert <?php echo ($alertBox ? 'alert-'.$alertBox['type'] : '' ); ?>">
      <a id="alertClose" class="close">×</a>
      <p id="alertMsg"><?php echo $alertBox['msg']; ?></p>
    </div>
  </div>

  <body>
    <div id="mainContainer">
      <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/"><?php echo $GLOBALS['sitename'];?></a>
            <div class="nav-collapse">
              <ul class="nav">
              <?php
                if(isLogedIn()) :
              ?>
                <li <?php if($ViewConfig['PageType'] === 'hiddenPage') echo 'class="active"' ?>><a href="/">Home</a></li>
                <li><a href="/users/logout">Logout</a></li>
              
              <?php
                elseif($ViewConfig['PageType'] !== 'login') :
              ?>
                <li><a href="/">Home</a></li>
              <?php
                endif;
              ?>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </div>
      </div>
    
      <div id="main">
      <?php
        switch($ViewConfig['PageType'])
        {
          case 'login':
            require_once(APP_PATH.'views/landing.php');
            break;
          case 'hiddenPage':
            require_once(APP_PATH.'views/hiddenPage.php');
            break;
          case 'about':
            require_once(APP_PATH.'views/about.php');
            break;
        }
      ?>     
      </div><!-- /main -->
    </div> <!-- /mainContainer -->
    <footer>
      <hr>
      <div class="container">
        <div class="row">
          <span class="span2">
            <p>Matthew Sonier &copy; 2012</p>
          </span>
          <span class="span10 textRight">
            <a href="/about">About</a>
            <a href="mailto:matt.sonier@gmail.com" target="_blank" >Contact</a>
          </span>
        </div>
      </div>
    </footer>
     
    <script src="<?php echo WEB_FOLDER ?>js/jquery-1.7.min.js" type="text/javascript"></script>
    <script src="<?php echo WEB_FOLDER ?>js/bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo WEB_FOLDER ?>js/main.js" type="text/javascript"></script>
  </body>
</html>

