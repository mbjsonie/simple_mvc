<div id="landingTop">
  <div id="navSpacing"></div>
  <div class="container">
    
    <div class="row">
      <span class="span9">
        <br />
        <h1 class="bigQuote">Useless but lightweight</h1>
        <br />
        <h1 class="bigQuote">Open Source</h1>
        <br />
        <h1 class="bigQuote">Simple and Easy</h1>
        <br />
        <h1 class="bigQuote">MVC framework for PHP</h1>
      </span>
      <span class="span3">
        <h3 class="formTitle">Log in</h3>
        <form id="loginForm" method="post" name="login">
          <input type="email" class="getFocus" placeholder="uWaterloo email" size="30" maxlength="128" id="LogInEmail" name="username" AUTOCOMPLETE="on">
          <input type="password" placeholder="Password" size="30" id="LogInPassword">
          
          <button class="btn-success btn" id="LogIn" type="button">Log In</button>
        </form>
        <div class="clear"></div>
        <h3 class="formTitle">Sign Up</h3>
        <form id="registerForm" method="post" name="register">
          <input type="email" placeholder="uWaterloo email" size="30" maxlength="128" id="SignUpEmail">
          <input type="password" placeholder="Password" size="30" id="SignUpPassword" >
          <input type="password" placeholder="Confirm" size="30" id="SignUpConfirm" >
          
          <button class="btn-success btn" id="SignUp" type="button">Sign Up</button>
        </form>
      </span>
    </div> <!-- /row -->
  </div> <!-- /container -->
</div>
<br />
<div class="container">
  <div class="row">
    <span class="span3">
      <h2>Simple</h2>
      <p>All in a few hundread lines.</p>
    </span>
    <span class="span3">
      <h2>Fast</h2>
      <p>No framework bloat</p>
    </span>
    <span class="span3">
      <h2>Flexible</h2>
      <p>PHP's PDO allows for database agnostic code.</p>
    </span>
    <span class="span3">
      <h2>Clean</h2>
      <p>MVC design pattern creates simple and clean code. SimpleMVC also creates lean urls!</p>
    </span>
    <span class="span3">
      <h2>Bootstrap</h2>
      <p>Built with <a href="http://twitter.github.com/bootstrap/index.html">Twitter Bootstrap</a> to help create easy UI.</p>
    </span>
    <span class="span3">
      <h2>Ajax</h2>
      <p>Supports ajax to simply load dynamic content</p>
    </span>
    <span class="span3">
      <h2>Log in</h2>
      <p>Email: test@test.com</p>
      <p>Password: testing</p>
      <p>Or sign up for a user authentication demo.</p>
    </span>
    <span class="span3">
      <h2>Useless</h2>
      <p>Boaring framework, you should probably use a real framework like Rails or something.</p>
    </span>
  </div> <!-- /row -->
</div><!-- /container -->
