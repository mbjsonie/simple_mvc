<div id="navSpacing"></div>
<div class="container">
  <div class="row">
    <div class="span12">
      <h1>About</h1>
      <br />
      <p>This open source software is released under the MIT license, the source code can be found on BitBucket?</p>
      <br />
      <p>Originally based on <a href="http://kissmvc.com/">KISSMVC</a>, it has been modified by <a href="http://mattsonier.com/">Matthew Sonier</a></p>
      <br />
      <h2>Changes</h2>
      <ul>
        <li>Support for compound primary keys</li>
      </ul>
    </div>
  </div>
</div> <!-- /container -->
