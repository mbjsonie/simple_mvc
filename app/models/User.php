<?php
class User extends Model
{
  function User()
  {
  parent::__construct(array('UserId'),'Users','getdbh');
  
  $this->rs['UserId'] = '';
  $this->rs['Email'] = '';
  $this->rs['Password'] = '';
  $this->rs['DateJoined'] = '';
  $this->rs['EmailConfirmed'] = '';
  $this->rs['ConfirmationCode'] = '';
  $this->rs['LastActivity'] = '';
  $this->rs['FailedLogins'] = '';
  }
  
  function updateActivity()
  {
    if($this->exists())
    {
      $this->set('LastActivity',getSQLDateTime());
      $this->update();
    }
  }
  
  function incrementFailedLogins()
  {
    if($this->exists())
    {
      $this->set('FailedLogins',$this->get('FailedLogins') + 1);
      $this->update();
    }
  }
  
  function resetFailedLogins()
  {
    if($this->exists())
    {
      $this->set('FailedLogins', 0);
      $this->update();
    }
  }
}
